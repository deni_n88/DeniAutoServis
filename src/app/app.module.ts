import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {BsDropdownModule} from 'ngx-bootstrap';
import {TooltipModule} from 'ngx-bootstrap';
import {ModalModule} from 'ngx-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {EmployeesComponent} from './employees/employees.component';
import {CarsComponent} from './cars/cars.component';
import {DefectsComponent} from './defects/defects.component';
import {EmployeeService} from './core/employee.service';
import {EmployeeDetailsComponent} from './employee-details/employee-details.component';
import {CarDetailsComponent} from './car-details/car-details.component';
import {AddEmployeeComponent} from './add-employee/add-employee.component';
import {AddCarComponent} from './add-car/add-car.component';
import {AddDefectComponent} from './add-defect/add-defect.component';
import {EditCarComponent} from './edit-car/edit-car.component';
import {CarService} from './core/car.service';
import {DefectService} from './core/defect.service';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    EmployeesComponent,
    CarsComponent,
    DefectsComponent,
    EmployeeDetailsComponent,
    CarDetailsComponent,
    AddEmployeeComponent,
    AddCarComponent,
    AddDefectComponent,
    EditCarComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot([
      {
        path: '',
        component: EmployeesComponent,
      },
      {
        path: 'cars',
        component: CarsComponent
      },
      {
        path: 'defects',
        component: DefectsComponent
      },
      {
        path: 'employee/:id',
        component: EmployeeDetailsComponent
      },
      {
        path: 'employee/:id/car/add',
        component: AddCarComponent
      },
      {
        path: 'employee/:id/car/:carId',
        component: CarDetailsComponent
      },
      {
        path: 'add/employee',
        component: AddEmployeeComponent
      },
      {
        path: 'add/defects/:carId/:employeeId',
        component: AddDefectComponent,
      },
      {
        path: 'edit/car/:id',
        component: EditCarComponent
      }
    ])
  ],
  providers: [EmployeeService, EmployeesComponent, CarService, DefectService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
