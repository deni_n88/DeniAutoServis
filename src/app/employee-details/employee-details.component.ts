import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EmployeeService} from '../core/employee.service';
import {CarService} from '../core/car.service';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  employee: any;

  constructor(private route: ActivatedRoute,
              private employeeService: EmployeeService,
              private carService: CarService,
              private router: Router) {
  }

  cars: any;
  defects: any;
  delete: Boolean = false;
  showDetails: Boolean = false;
  allCars: any;
  carModel: string;
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.employeeService.getEmployee(params.id).subscribe(employee => {
        this.employee = employee;
        this.cars = employee.cars.reverse();
        this.defects = employee.defects;
      });
    });
    this.carService.getCars().subscribe(allcars => {
      this.allCars = allcars.reverse();
    });
  }

  deleteEmployee() {
    this.employeeService.delEmployee(this.employee.id).subscribe(data => {
      this.router.navigate(['']);
    });
  }

  delConfirm() {
    this.delete = true;
  }

  delDecline() {
    this.delete = false;
  }

  toggleShowDetails() {
    this.showDetails = !this.showDetails;
  }

  searchCars() {
    if (this.carModel != null && this.carModel.length > 0) {

      this.carService.getCarsByCarModel(this.carModel).subscribe(list => {
        this.allCars = list;
      });
    } else {
      this.carService.getCars().subscribe(list => {
        this.allCars = list.reverse();
      });
    }
  }
}
