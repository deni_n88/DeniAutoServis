import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {Defect} from '../models/Defect';

@Injectable({
  providedIn: 'root'
})
export class DefectService {

  constructor(private http: HttpClient) {
  }

  addDefect(carId: number, employeeId: number, defect: Defect) {
    return this.http.post('http://localhost:8080/defects/post/' + carId + '/' + employeeId, defect);
  }
}
