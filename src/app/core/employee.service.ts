import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) {
  }

  getEmployees(): Observable<any> {
    return this.http.get('http://localhost:8080/employees/');
  }

  getEmployee(id): Observable<any> {
    return this.http.get('http://localhost:8080/employees/' + id);
  }

  addEmployee(name: any): Observable<any> {
    return this.http.post('http://localhost:8080/employees/' + name, {});
  }

  delEmployee(id): Observable<any> {
    return this.http.delete('http://localhost:8080/employees/' + id);
  }

}
