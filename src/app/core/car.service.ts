import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Car} from '../models/Car';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private http: HttpClient) {
  }


  getCars(): Observable<any> {
    return this.http.get('http://localhost:8080/cars/');
  }

  getCarsByCarModel(carModel: string): Observable<any> {
    return this.http.get('http://localhost:8080/cars/find/' + carModel);
  }

  getCarsByLicencePlate(licencePlate: string): Observable<any> {
    return this.http.get('http://localhost:8080/cars/find/licencePlate/' + licencePlate);
  }


  getCar(id): Observable<any> {
    return this.http.get('http://localhost:8080/cars/' + id);
  }

  addCar(id: number, car: Car) {
    return this.http.post('http://localhost:8080/cars/post/' + id, car);
  }

  editCar(carId: number, car: Car) {
    return this.http.put('http://localhost:8080/cars/edit/' + carId, car);
  }

  delCar(carId: number): Observable<any> {
    return this.http.delete('http://localhost:8080/cars/delete/' + carId);
  }
}
