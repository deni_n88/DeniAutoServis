import {Component, OnInit} from '@angular/core';
import {EmployeeService} from '../core/employee.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CarService} from '../core/car.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.css']
})
export class AddCarComponent implements OnInit {

  licencePlatePattern = '[A-Z][0-9]{2}[-][A-Z][-][0-9]{3}';

  selectedEmployee: any;

  licencePlateInDatabase: Boolean = false;

  newCar = new FormGroup({
    carModel: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20)
    ]),
    licencePlate: new FormControl('', [
      Validators.required,
      Validators.pattern(this.licencePlatePattern)
    ])
  });

  constructor(private employeeService: EmployeeService,
              private carService: CarService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.employeeService.getEmployee(params.id).subscribe(employee => {
        this.selectedEmployee = employee;
      });
    });
  }

  onSubmit() {
    this.carService.addCar(this.selectedEmployee.id, this.newCar.value).subscribe(value => {
      this.router.navigate(['/employee', this.selectedEmployee.id]);
    });
  }

  checkDatabase() {
    if (this.newCar.valid) {
      this.carService.getCarsByLicencePlate(this.newCar.get('licencePlate').value).subscribe(car => {
        if (car !== null) {
          this.licencePlateInDatabase = true;
        } else {
          this.licencePlateInDatabase = false;
        }
      });
    } else {
      this.licencePlateInDatabase = false;
    }
  }

  back() {
    this.router.navigate(['/employee', this.selectedEmployee.id]);
  }
}
