export class Car {
  carModel: string;
  licencePlate: string;

  constructor(carModel: string, licencePlate: string) {
    this.carModel = carModel;
    this.licencePlate = licencePlate;
  }
}
