export class Defect {
  details: string;
  cost: number;

  constructor(details: string, cost: number) {
    this.details = details;
    this.cost = cost;
  }
}
