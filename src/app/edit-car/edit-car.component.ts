import {Component, enableProdMode, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EmployeeService} from '../core/employee.service';
import {CarService} from '../core/car.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-edit-car',
  templateUrl: './edit-car.component.html',
  styleUrls: ['./edit-car.component.css']
})
export class EditCarComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private employeeService: EmployeeService,
              private carService: CarService) {
  }

  licencePlatePattern = '[A-Z][0-9]{2}[-][A-Z][-][0-9]{3}';
  car: any;
  carModel: string;
  licencePlate: string;
  delete: Boolean = false;

  editCar = new FormGroup({
    carModel: new FormControl('', [
      Validators.required
    ]),
    licencePlate: new FormControl('', [
      Validators.required,
      Validators.pattern(this.licencePlatePattern)
    ])
  });

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.carService.getCar(params.id).subscribe(car => {
        this.car = car;
        this.editCar.get('carModel').setValue(car.carModel);
        this.editCar.get('licencePlate').setValue(car.licencePlate);
      });
    });
  }

  updateCar() {
    this.carService.editCar(this.car.id, this.editCar.value).subscribe(data => {
      this.router.navigate(['/cars']);
    });
  }

  delConfirm() {
    this.delete = true;
  }

  delDecline() {
    this.delete = false;
  }

  deleteCar() {
    this.carService.delCar(this.car.id).subscribe(data => {
      this.router.navigate(['/cars']);
    });
  }

  back() {
    this.router.navigate(['/cars']);
  }
}
