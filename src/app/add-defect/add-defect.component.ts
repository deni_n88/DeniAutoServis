import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EmployeeService} from '../core/employee.service';
import {DefectService} from '../core/defect.service';
import {CarService} from '../core/car.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-defect',
  templateUrl: './add-defect.component.html',
  styleUrls: ['./add-defect.component.css']
})
export class AddDefectComponent implements OnInit {

  employee: any;
  car: any;
  defect = new FormGroup({
    details: new FormControl('', [
      Validators.required,
      Validators.minLength(2)
    ]),
    cost: new FormControl('', [
      Validators.required,
      Validators.maxLength(10)
    ])
  });

  constructor(private route: ActivatedRoute,
              private carService: CarService,
              private employeeService: EmployeeService,
              private defectService: DefectService,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.carService.getCar(params.carId).subscribe(car => {
        this.car = car;
      });
    });
    this.route.params.subscribe(params => {
      this.employeeService.getEmployee(params.employeeId).subscribe(employee => {
        this.employee = employee;
      });
    });
  }

  postDefect() {
    this.defectService.addDefect(
      this.car.id,
      this.employee.id,
      this.defect.value).subscribe(value => {
      this.router.navigate(['/employee/', this.employee.id, 'car', this.car.id]);
    });
  }

  back() {
    this.router.navigate(['/employee/', this.employee.id, 'car', this.car.id]);
  }
}

