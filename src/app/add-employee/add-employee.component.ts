import {Component, OnInit} from '@angular/core';
import {EmployeeService} from '../core/employee.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  constructor(private employeeService: EmployeeService, private router: Router) {
  }

  newEmployee = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ])
    }
  );

  ngOnInit() {
  }

  postEmployee() {
    this.employeeService.addEmployee(this.newEmployee.get('name').value).subscribe(data => {
      this.router.navigate(['']);
    });
  }
}
