import {Component, OnInit} from '@angular/core';
import {EmployeeService} from '../core/employee.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
})
export class EmployeesComponent implements OnInit {

  employees: Array<any>;

  constructor(private employeeService: EmployeeService) {
  }

  ngOnInit() {
    this.employeeService.getEmployees().subscribe(value => {
      this.employees = value;
    });
  }
}

