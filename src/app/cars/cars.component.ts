import {Component, OnInit} from '@angular/core';
import {CarService} from '../core/car.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  carList: Array<any>;
  carModel: string;

  constructor(private carService: CarService) {
  }

  ngOnInit() {
    this.carService.getCars().subscribe(data => {
      this.carList = data;
      this.carList = this.carList.reverse();
    });
  }

  getCarsByCarName() {
    if (this.carModel != null && this.carModel.length > 0) {

      this.carService.getCarsByCarModel(this.carModel).subscribe(data => {
        this.carList = data;
      });
    } else {
      this.ngOnInit();
    }
  }
}
