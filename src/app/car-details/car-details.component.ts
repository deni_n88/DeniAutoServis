import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EmployeeService} from '../core/employee.service';
import {CarService} from '../core/car.service';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.css']
})
export class CarDetailsComponent implements OnInit {

  car: any;
  employee: any;

  constructor(private route: ActivatedRoute,
              private employeeService: EmployeeService,
              private carService: CarService,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.employeeService.getEmployee(params.id).subscribe(employee => {
        this.employee = employee;
      });
    });
    this.route.params.subscribe(params => {
      this.carService.getCar(params.carId).subscribe(car => {
        this.car = car;
      });
    });
  }

  back() {
    this.router.navigate(['/employee/', this.employee.id]);
  }
}
